from django.apps import AppConfig


class TalentohConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'talentoh'
