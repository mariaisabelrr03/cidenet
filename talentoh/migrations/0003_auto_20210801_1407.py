# Generated by Django 3.2.3 on 2021-08-01 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('talentoh', '0002_alter_empleados_pais_empleo'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empleados',
            name='numero_identificacion',
            field=models.CharField(max_length=20),
        ),
        migrations.AlterField(
            model_name='empleados',
            name='tipo_identificacion',
            field=models.CharField(choices=[('1', 'Cédula de Ciudadanía'), ('2', 'Cédula de Extranjería'), ('3', 'Pasaporte'), ('4', 'Permiso Especial')], max_length=20),
        ),
    ]
