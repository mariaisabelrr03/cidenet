from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from .models import Empleados

# Create your views here.
from django.http import HttpResponse

def consultar_empleados(request):
    q = Empleados.objects.all()
    contexto = {'datos': q}
    return render(request, 'talentoh/index.html', contexto)

def FormularioActualizar(request, id):
    if id == 0:
        q = {"dato": 0}
    else:
        q = Empleados.objects.get(pk = id)
    contexto = { "dato": q }
    return render(request, 'talentoh/formulario_actualizar.html', contexto)

def crear_empleado(request):
    
    primer_apellido = request.POST['primer_apellido'].upper()
    segundo_apellido = request.POST['segundo_apellido'].upper()
    primer_nombre = request.POST['primer_nombre'].upper()
    otros_nombres = request.POST['otros_nombres'].upper()
    pais_empleo = request.POST['pais_empleo']
    tipo_identificacion = request.POST['tipo_identificacion']
    numero_identificacion = request.POST['numero_identificacion']
    fecha_ingreso = request.POST['fecha_ingreso']
    area = request.POST['area']
    estado = request.POST['estado']
    fecha_registro = request.POST['fecha_registro']
    empleado = Empleados(primer_apellido=primer_apellido, segundo_apellido=segundo_apellido, 
                         primer_nombre= primer_nombre, otros_nombres=otros_nombres, pais_empleo=pais_empleo, 
                         tipo_identificacion=tipo_identificacion, numero_identificacion=numero_identificacion, 
                         fecha_ingreso=fecha_ingreso
                         , area=area, estado=estado, fecha_registro=fecha_registro )
    
    empleado.save()
    id= Empleados.objects.latest("id").pk
    q = Empleados.objects.get(pk = id)
    if q.pais_empleo == "Colombia":
        q.Correo_Electronico= primer_nombre+"."+primer_apellido+"."+str(id)+"@CIDENET.COM.CO"
    else:
        q.Correo_Electronico= primer_nombre+"."+primer_apellido+"."+str(id)+"@CIDENET.COM.US    "
    q.save()
    return HttpResponseRedirect(reverse('talentoh:consultar_empleados', args=() ))

def actualizar(request, id):
    q = Empleados.objects.get(pk = id)

    q.primer_apellido = request.POST['primer_apellido']
    q.segundo_apellido = request.POST['segundo_apellido']
    q.primer_nombre = request.POST['primer_nombre']
    q.otros_nombres = request.POST['otros_nombres']
    q.pais_empleo = request.POST['pais_empleo']
    q.tipo_identificacion = request.POST['tipo_identificacion']
    q.numero_identificacion = request.POST['numero_identificacion']
    q.fecha_ingreso = request.POST['fecha_ingreso']
    q.area = request.POST['area']
    q.estado = request.POST['estado']
    q.save()
    return HttpResponseRedirect(reverse('talentoh:consultar_empleados', args=() ))

def EmpleadoEliminar(request, id):
    q = Empleados.objects.get(pk = id)
    q.delete()
    return HttpResponseRedirect(reverse('talentoh:consultar_empleados', args=() ))

def validarNumero(request):
    empleado = Empleados.objects.get(numero_identificacion=request.POST['numero_identificacion']).pk
    return HttpResponse(empleado>0)
    