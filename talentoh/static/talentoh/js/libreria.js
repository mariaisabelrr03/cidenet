//mostrar las fechas en la creaciòn y actualizaciòn de los datos
$(document).ready(function() {
    var id = $("#empleadoId").val()
    var fecha = new Date()
    var fechafinal = fecha.getFullYear() + "-" + pad(fecha.getMonth() + 1) + "-" + pad(fecha.getDate()) + "T" + pad(fecha.getHours()) + ":" + pad(fecha.getMinutes()) + ":" + pad(fecha.getSeconds())
    if (id == "") {
        $("#fecha_registro").val(fechafinal)
        $("#fecha_ingreso").val(fechafinal)
    }
    $("#fecha_ingreso").attr("max", fechafinal)
    $("#fecha_ingreso").attr("min", fecha.getFullYear() + "-" + pad(fecha.getMonth()) + "-" + pad(fecha.getDate()) + "T" + pad(fecha.getHours()) + ":" + pad(fecha.getMinutes()) + ":" + pad(fecha.getSeconds()))
});

function pad(num) {
    if (num < 10) {
        return "0" + num
    } else {
        return num
    }
}

function eliminarEmpleado(ruta) {
    if (confirm('Está seguro de que desea eliminar el empleado?')) {
        location.href = ruta;
        $.ajax({
            url: (ruta),
            success: function(result) {
                alert("empleado eliminado")
            }
        });
    }
}

function check(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros y letras
    patron = /[A-Za-z]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
// para evitar guardar sin llenar los campos del formulario
function validar(id) {
    var primer_nombre = document.getElementById("primer_nombre").value
    var primer_apellido = document.getElementById("primer_apellido").value
    var segundo_apellido = document.getElementById("segundo_apellido").value
    var numero_identificacion = document.getElementById("numero_identificacion").value
    var fecha_ingreso = document.getElementById("fecha_ingreso").value
    var tipo_identificacion = document.getElementById("tipo_identificacion").value
    var Correo_Electronico = document.getElementById("Correo_Electronico").value
    var pais_empleo = document.getElementById("pais_empleo").value
    var area = document.getElementById("area").value
    var estado = document.getElementById("estado").value
    var otros_nombres = document.getElementById("otros_nombres").value
    var fecha_registro = document.getElementById("fecha_registro").value
    if (primer_nombre == "" || primer_nombre == null) {
        document.getElementById("primer_nombre").focus()
        alert("debe ingresar el primer nombre")
    } else if (primer_apellido == "" || primer_apellido == null) {
        document.getElementById("primer_apellido").focus()
        alert("debe ingresar el primer apellido")
    } else if (segundo_apellido == "" || segundo_apellido == null) {
        document.getElementById("segundo_apellido").focus()
        alert("debe ingresar el segundo apellido")
    } else if (numero_identificacion == "" || numero_identificacion == null) {
        document.getElementById("numero_identificacion").focus()
        alert("debe ingresar el número de identificación")
    } else if (fecha_ingreso == "" || fecha_ingreso == null) {
        document.getElementById("fecha_ingreso").focus()
        alert("debe ingresar la fecha de ingreso")
    } else if (fecha_registro == "" || fecha_registro == null) {
        document.getElementById("fecha_registro").focus()
        alert("debe ingresar la fecha de registro")
    } else {
        var url = "../../actualizar/" + id + "/"
        if (id == 0) {
            url = "../../crear_empleado/"
        }
        $.ajax({
            headers: { "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").val() },
            url: url,
            type: "post",
            data: {
                primer_nombre: primer_nombre,
                primer_apellido: primer_apellido,
                segundo_apellido: segundo_apellido,
                otros_nombres: otros_nombres,
                pais_empleo: pais_empleo,
                tipo_identificacion: tipo_identificacion,
                numero_identificacion: numero_identificacion,
                Correo_Electronico: Correo_Electronico,
                area: area,
                estado: estado,
                fecha_registro: fecha_registro,
                fecha_ingreso: fecha_ingreso
            },
            success: function(response) {
                alert("empleado guardado")
                window.location = "../../"
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert(textStatus + errorThrown);
            }
        });

    }
}
// Cuando se escribe el nùmero de documento igual a uno existente
function validarNumero() {
    var numero_identificacion = document.getElementById("numero_identificacion").value
    $.ajax({
        headers: { "X-CSRFToken": $("input[name='csrfmiddlewaretoken']").val() },
        url: "../../validarNumero/",
        type: "post",
        data: {
            numero_identificacion: numero_identificacion
        },
        success: function(response) {
            if (response) {
                alert("El número de identificación ya existe")
                $("#numero_identificacion").focus()
                $("#numero_identificacion").val("")
            }

        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });
}