from django.contrib import admin

# Register your models here.
from .models import Empleados
# Register your models here.

class EmpleadosAdmin(admin.ModelAdmin):
    list_display=('primer_apellido','primer_nombre',)

admin.site.register(Empleados, EmpleadosAdmin) 