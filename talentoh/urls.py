from django.urls import path

from . import views

app_name="talentoh"

urlpatterns = [
    path('', views.consultar_empleados, name='consultar_empleados'),
    path('formulario_actualizar/<int:id>/', views.FormularioActualizar, name='formulario_actualizar'),
    path('actualizar/<int:id>/', views.actualizar, name='actualizar'),
    path('EmpleadoEliminar/<int:id>/', views.EmpleadoEliminar, name='EmpleadoEliminar'),
    path('crear_empleado/', views.crear_empleado, name='crear_empleado'),
    path('validarNumero/', views.validarNumero, name='validarNumero'),
]