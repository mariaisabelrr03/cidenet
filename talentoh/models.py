from django.db import models

# Create your models here.

class Empleados(models.Model):
    primer_apellido = models.CharField(max_length = 20, null=False, blank=False)
    segundo_apellido = models.CharField(max_length = 20, null=False, blank=False)
    primer_nombre = models.CharField(max_length = 20, null=False, blank=False)
    otros_nombres = models.CharField(max_length = 20,  blank=True)
    PAISEMPLEO = (
        ('Colombia', 'Colombia'),
        ('EE.UU', 'EE.UU'),  
    )
    pais_empleo = models.CharField(max_length=30, choices=PAISEMPLEO, null=False, blank=False)
    
    numero_identificacion = models.CharField(max_length=20, null=False, blank=False, unique=True)
    TIPO = (
        ('Cédula de Ciudadanía', 'Cédula de Ciudadanía'),
        ('Cédula de Extranjería', 'Cédula de Extranjería'),
        ('Pasaporte', 'Pasaporte'),
        ('Permiso Especial', 'Permiso Especial'),
    )
    tipo_identificacion = models.CharField(max_length=21, choices=TIPO, null=False, blank=False)
    Correo_Electronico=models.EmailField(max_length=300, null=False, blank=False)
    fecha_ingreso =  models.DateTimeField(auto_now=False, auto_now_add=False, null=False, blank=False)
    AREA = (
        ('Administración', 'Administración'),
        ('Financiera', 'Financiera'),
        ('Compras', 'Compras'),
        ('Infraestructura', 'Infraestructura'),
        ('Operación', 'Operación'),
        ('Talento Humano', 'Talento Humano'),
        ('Servicios Varios', 'Servicios Varios'),
    )
    area = models.CharField(max_length=30, choices=AREA, null=False, blank=False)
    
    ESTADO = (
        ('Activo', 'Activo'),
        ('Inactivo', 'Inactivo'),
    )
    estado = models.CharField(max_length=30, choices=ESTADO, default='1', null=False, blank=False)
    fecha_registro = models.DateTimeField(auto_now=False, auto_now_add=False, blank=False)
    
    class Meta:
        verbose_name="Empleado"
        verbose_name_plural="Empleados"
    
    def __str__(self):
        return f"Nombre: {self.primer_nombre} - Apellido: {self.primer_apellido}"

